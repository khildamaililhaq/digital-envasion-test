class SendGiftJob < ApplicationJob
  queue_as :default

  def perform(user)
    send_gift = SendGiftService.new(user)
    send_gift.call
  end
end
