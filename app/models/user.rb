class User < ApplicationRecord
  after_create :create_birthday_job
  def zonal_time
    Timezone.lookup(lat, long)
  end

  def next_birthday
    year = Date.today.year
    mmdd = birthday.strftime('%m%d')
    year += 1 if mmdd < Date.today.strftime('%m%d')
    mmdd = '0301' if mmdd == '0229' && !Date.parse("#{year}0101").leap?
    Date.parse("#{year}#{mmdd}")
  end

  def utc_birthday
    zonal_time.time next_birthday.beginning_of_day
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def create_birthday_job
    SendGiftJob.set(wait_until: utc_birthday).perform_later(self)
  end
end
