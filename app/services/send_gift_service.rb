class SendGiftService
  def initialize(user)
    @user = user
    @payload = {
      message: "Hey #{@user.full_name}, its your birthday!"
    }
  end

  def call
    @api = RestClient.post(ENV['BIN_URL'], @payload)

    SendGiftJob.set(wait_until: @user.utc_birthday).perform_later(@user) if @api.code == 200
  end
end
