This App using
- PostgreSQL as Database
- Redis
- Sidekiq
Ive been packed all with docker to make the setup easy and consistent

Make sure you already set the ENV file according to env.example file provided, dont forget to change the BIN_URL to yours

To setup rails app after you setup, run

```shell
docker-compose build
```

and then run:

```shell
docker-compose run web rake db:setup
```

after that you can run the apps instance locally using

```shell
docker-compose up
```

to access the swagger for the API test you can use the url
```shell
localhost:3000/api-docs
```
and you can look at the your Hookbin repository
