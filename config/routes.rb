Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'
  resources :users, except: :update
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
